odoo.define('wp_override.paymentScreenExtension', function (require) {
    'use strict';

    const PaymentScreen = require('point_of_sale.PaymentScreen');
    const Registries = require('point_of_sale.Registries');
    var core = require('web.core');
    var _t = core._t;

    const ExtendedPaymentScreen = (PaymentScreen) => {
        class ExtendedPaymentScreen extends PaymentScreen {
            constructor() {
                super(...arguments);
//                this.onSendWhatsApp = this.onSendWhatsApp.bind(this);
            }

            async onSendWhatsApp() {
                const order = this.currentOrder;
                const client = order && order.get_client();
                let phoneNumber = '';
                debugger;
                // Get the phone number
                if (client && client.phone) {
                    phoneNumber = client.phone;
                } else {
                    // Handle invalid phone number
                    return this.showPopup('ErrorPopup', {
                        title: _t('Phone Number is Required'),
                        body: _t('Please add phone number in customer'),
                    });
                }

                // Construct the message
                const clientName = client ? client.name : '';
                const clientAddress = client ? client.address : '';
                const clientEmail = client ? client.email : '';
                const orderNumber = order ? order.name : '';
                const orderDate = order ? new Date(order.creation_date).toLocaleDateString() : '';
                let productDetails = '';
                let totalQuantity = 0;

                order.get_orderlines().forEach(line => {
                    productDetails += `${line.product.display_name}\n(Quantity x${line.quantity} - Rate: ${this.env.pos.format_currency(line.get_unit_price())} - Total: ${this.env.pos.format_currency(line.get_price_with_tax())})\n\n`;
                    totalQuantity += line.quantity;
                });

                const orderTotal = order ? this.env.pos.format_currency(order.get_total_with_tax()) : '';

                // Additional company info
                const companyPhoneNumber = '7874131444';
                const companyGSTNumber = '24AABFC4BS3C1ZY';
                const companyWebsite = 'http://www.chakhdi.in/';
                const termsAndConditions = '(1) No Guarantee\n(2) No Refund Policy\n(3) Exchange Time Within 8 Days'; // Replace this with your actual terms and conditions

                const message = `Dear ${clientName},\n\n`
                              + `Thank you for your order!\n`
                              + `Order Number: ${orderNumber}\n`
                              + `Order Date: ${orderDate}\n`
                              + `Your order details:\n`
                              + `----------------------------------\n`
                              + `${productDetails}\n`
                              + `----------------------------------\n`
                              + `Total Amount: ${orderTotal}\n\n`
                              + `Total Quantity: ${totalQuantity}\n`
                              + `Telephone Number: ${companyPhoneNumber}\n`
                              + `GST Number: ${companyGSTNumber}\n`
                              + `Website: ${companyWebsite}\n\n`
                              + `Terms and Conditions:\n${termsAndConditions}\n\n`
                              + `We appreciate your business!\n\n`
                              + `Best regards,\n`
                              + this.env.pos.company.name;

                // Send the WhatsApp message
                try {
                    const whatsappUrl = "https://wa.me/" + phoneNumber + "?text=" + encodeURIComponent(message);
                    window.open(whatsappUrl, '_blank');
                } catch (error) {
                    console.error('Failed to send WhatsApp message:', error);
                    // Handle error
                }
            }
        }

        Registries.Component.add(ExtendedPaymentScreen);

        return ExtendedPaymentScreen;
    }

    Registries.Component.extend(PaymentScreen, ExtendedPaymentScreen);

    return ExtendedPaymentScreen;
});
