FROM odoo:17.0

USER root

ENV DEBIAN_FRONTEND=noninteractive \
    LANG=C.UTF-8

#COPY ./entrypoint.sh /

RUN apt-get update \
    && apt-get install --no-install-recommends -y python3-passlib  python3-wheel \
     openssl build-essential libssl-dev libxrender-dev git-core libx11-dev libxext-dev \
     libfontconfig1-dev libfreetype6-dev fontconfig  \
    && apt-get install python3.10-dev -y\
    && rm -f /var/lib/apt/lists/*.*\ 
    && pip install --no-cache-dir manifestoo

USER odoo



FROM docker:19.03.12

# Install Docker inside the Docker container
RUN apk add --no-cache docker-cli